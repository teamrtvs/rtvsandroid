package mam.keti.kr.mamclient;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebView.WebViewTransport;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.widget.ProgressBar;
import android.webkit.ValueCallback;
import android.webkit.WebSettings.PluginState;
import android.webkit.JavascriptInterface;
import android.net.Uri;
import android.content.res.Configuration;
import java.lang.reflect.Method;
import java.util.UUID;
import android.media.MediaMetadataRetriever;
//import java.nio.ByteBuffer;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;

public class MAMmain extends Activity {

    WebView webview;
    ChromeClient chromeClient;
    //    ProgressBar progressBar;
    MyFTPClient ftpclient = null;
    private Context ctx = null;
    private String UserAgent = "";
    private String phoneModel = "";

    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE=1;
    private String lastSOP = "";
    private String theFilePath = null;
    private String theURLPath = "";
    private String ftpfileName = null;
    private String ftpfilePath = null;

    private MenuItem item;

    Context context;
    long backPressTime = 0;
    float CPU;
    boolean loadingFinished = true;
    boolean redirect = false;
    int BatteryLevel = 0;
    boolean IsLoaded_WebView = false;
    public double long1=0, lat1=0;
    private ProgressDialog pbar;
    private boolean ProgressDismissed = false;
    private String ServerIP = "221.143.42.141";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /// 화면 레이아웃 결정
        setContentView(R.layout.activity_mammain);
        if(savedInstanceState != null) IsLoaded_WebView = true;
        /// 모델명 결정
        phoneModel = Build.MODEL; // 패드 지원 SM-T700

        /// Orientation 정리
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        /// 웹뷰처리
        webview = (WebView)findViewById(R.id.webview);
        /// 크롬 클라이언트 정의
        chromeClient = new ChromeClient();

        /// 웹뷰의 다양한 세팅
        webview.clearFormData();
        webview.clearHistory();
        webview.clearCache(true);
        WebSettings settings = webview.getSettings();
        settings.setLoadsImagesAutomatically(true);
        /// 자바 스크립트 가능하도록
        settings.setJavaScriptEnabled(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        /// 파일 접근 가능
        settings.setAllowFileAccess(true);
        /// 콘텐츠 접근 가능
        settings.setAllowContentAccess(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        /// 줌 컨트롤 가능
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        /// 줌 버튼은 보이지 않게
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);

        methodInvoke(settings, "setPluginsEnabled", new Class[] { boolean.class }, new Object[] { true });
        methodInvoke(settings, "setPluginState", new Class[] { PluginState.class }, new Object[] { PluginState.ON });
        methodInvoke(settings, "setPluginsEnabled", new Class[] { boolean.class }, new Object[] { true });
        methodInvoke(settings, "setAllowUniversalAccessFromFileURLs", new Class[] { boolean.class }, new Object[] { true });
        methodInvoke(settings, "setAllowFileAccessFromFileURLs", new Class[] { boolean.class }, new Object[] { true });

        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webview.addJavascriptInterface(chromeClient, "jsi");
        /// 자바 스크립트와의 Interface를 "app" 이라는 객체로 연결함.
        webview.addJavascriptInterface(new WebViewJavaScriptInterface(this), "app");
        webview.setWebViewClient(new WebClient()); // 응룡프로그램에서 직접 url 처리
        webview.setWebChromeClient(chromeClient);

//        setContentView(webview);
        // 배터리 리스너
        getBatteryPercentage();

//        webview.loadUrl("http://" + ServerIP + ":8080/SearchMain.aspx");

        try {
            /// CPU 사용량 가지고 우기
            CPU = readUsage();
            Log.i( "LOG",  "CPU: " + String.valueOf(CPU*100));
            //webview.loadUrl("javascript:CPU_usage('"+String.valueOf(CPU*100)+"')");
        }catch(Exception ex) {
            Log.i("MAMmain", ex.getMessage());
        }
        Log.i("MAM", "BatteryLevel: " + String.valueOf(BatteryLevel));

        context = this;
        ctx = this.getBaseContext();
    }

    @Override
    protected void onResume()
    {
        Log.i("MAM", "BatteryLevel: " + String.valueOf(BatteryLevel));
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // Save the state of the WebView
        webview.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        // Restore the state of the WebView
        webview.restoreState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_mammain, menu);

        // Java Code로 옵션메뉴 추가 하기
        menu.add(0, 1, 1, "External Service 221");
        menu.add(0, 2, 2, "Internal Service 192");
        menu.add(0, 3, 3, "Cloud Service");
//        item = menu.getItem(0);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    /// 옵션 메뉴를 선택했을 때의 처리 --> 세팅화면으로 가게
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case 1:
                ServerIP = "221.143.42.141";
                break;
            case 2:
                ServerIP = "192.168.96.80";
                break;
            case 3:
                ServerIP = "221.143.42.141";
                break;
        }

        String strUrl = "";
        if (phoneModel.contains("SM-T700")) {
            strUrl = "http://"+ServerIP+":3003/SearchResult.aspx?wa=on&BS=" + String.valueOf(BatteryLevel)
                    + "&CPU=" + String.valueOf((int) (CPU * 100)) + "&GPS=" + String.valueOf(long1) + "_" + String.valueOf(lat1);
        }
        else
        {
            strUrl = "http://"+ServerIP+":3003/SearchResult.aspx?wa=on&BS=" + String.valueOf(BatteryLevel)
                    + "&CPU=" + String.valueOf((int) (CPU * 100)) + "&GPS=" + String.valueOf(long1) + "_" + String.valueOf(lat1);
        }
        webview.loadUrl(strUrl);
        Log.i("MAMmain", "BS: " + strUrl);

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            CPU = readUsage();
//            getGPS();
//
//            String strUrl = "http://" + ServerIP+ ":8080/SearchMain.aspx?BS="+String.valueOf(BatteryLevel)
//                    + "&CPU=" + String.valueOf((int)(CPU*100))+"&GPS=" + String.valueOf(long1)+"_"+String.valueOf(lat1) + "&wa=on";
//            webview.loadUrl(strUrl);
//            Log.i("MAMmain", "BS: " + strUrl);
//
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    /// 백버튼을 두번 눌러야 앱이 종료되게
    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            if (backPressTime + 2000 > System.currentTimeMillis()) finish();
            else {
                Toast.makeText(context, "한번 더 누르시면 종료됩니다.",   Toast.LENGTH_SHORT).show();
                backPressTime = System.currentTimeMillis();
            }
        }
    }

    /// 다른 액티비티에서 돌아올 때: 여기서는 파일 Picker가 뜬 뒤에 파일 업로드를 수행하는 경우에 처리
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.i("MAMmain", "onActivityResult: " + String.valueOf(requestCode));
        if(intent == null) return;
        Uri result = null;
        /// 파일 선택을 하고 돌아온 경우
        if(requestCode==FILECHOOSER_RESULTCODE)
        {
            result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
            String str1 = result.getPath();
            String str2 = result.getLastPathSegment();
            Utils utils = new Utils();
            /// 파일의 실제 Path를 URI 값으로 부터 읽어옴
            String filePath = utils.getRealPathFromURI(MAMmain.this, result);
            /// 파일 이름만 추출함
            String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);

            /// 프로그레스 표시
            showProgress("Uploading");
            /// FTP로 업로드 함.
       //   uploadFTP(filePath, fileName);
           UploadFile(filePath,fileName);
            /// 업로드해서 업로드가 완료되어 접근 URL이 생성딜 때 까지 기다림.
            Handler handler = new Handler();
            while ( theURLPath == "") {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    }
                }, 1000);
            }
            /// 패드의 경우
            if(phoneModel.contains("SM-T700")) {
                Log.i("MAMmain", "http://"+ServerIP+":3003/SearchResult.aspx?wa=on&" + theURLPath);
                webview.loadUrl("http://"+ServerIP+":3003/SearchResult.aspx?wa=on&" + theURLPath);
            }
            /// 폰의 경우
            else {
                Log.i("MAMmain", "http://"+ServerIP+":3003/SearchResult.aspx?wa=on&" + theURLPath);
                webview.loadUrl("http://"+ServerIP+":3003/SearchResult.aspx?wa=on&" + theURLPath);
            }
            theURLPath = "";


//            item.setActionView(R.layout.progress);//            SendHttpRequestTask t = new SendHttpRequestTask();
//            String[] params = new String[]{url, fileName, filePath};
//            t.execute(params);
//            t = null;


//            mUploadMessage.onReceiveValue(result);
//            mUploadMessage = null;
        }
    }

    /// HTTP 파일 업로드
    public void UploadFile(String filePath, String fileName){

        HttpFileUpload hfu = new HttpFileUpload();

        fileName = "mvid_" + java.util.UUID.randomUUID().toString() + "_"+fileName;
        hfu.HttpFileUpload("http://" + ServerIP + ":3003/fileup.aspx", "my file title",filePath, fileName);
        theFilePath = "http://"+ServerIP+":3000/VideosUploaded/" + fileName;
        theURLPath = "sop=v&VideoSearch=" + theFilePath;

    }

    //flipscreen not loading again
    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    public class WebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            Log.i("MAMmain", "shouldOverrideUrlLoading: " + urlNewString);
            if (!loadingFinished) {
                redirect = true;
            }
            loadingFinished = false;
            view.loadUrl(urlNewString);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            super.onPageStarted(view, url, facIcon);
            if(pbar == null || !pbar.isShowing()) showProgress("Connecting: " + url);
            Log.i("MAMmain", "onPageStarted: " + url);
            loadingFinished = false;
            //SHOW LOADING IF IT ISNT ALREADY VISIBLE
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i("MAMmain", "onPageFinished: " + url );
            /// UserAgent를 가지고 옴.
            if (UserAgent.isEmpty()) UserAgent = view.getSettings().getUserAgentString();
            /// 검색 옵션 Parsing
            int lastIndex = url.lastIndexOf("sop=");
            if (lastIndex>=0) lastSOP = url.substring(lastIndex+4,lastIndex+5);

            /// 리다이렉트인 경우
            if(!redirect){
                loadingFinished = true;
            }
            /// 로딩이 끝나고 리다이렉트인 경우
            if(loadingFinished && !redirect){
                Log.i("MAMmain", "onPageFinished Not redirect: " + url );
                ProgressDismissed = true;
            } else {
                Log.i("MAMmain", "onPageFinished redirect: " + url );
                redirect = false;
            }
        }
    }

    // 배터리 상태를 Receiver로 받아서 업데이트함.
    private void getBatteryPercentage() {
        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (currentLevel >= 0 && scale > 0) {
                    level = (currentLevel * 100) / scale;
                }
                BatteryLevel = level;
                Log.i("MAM_onReceive: ", "BatteryLevel: " + String.valueOf(BatteryLevel));
                Log.i("MAM", "====");
                if(!IsLoaded_WebView){
                    IsLoaded_WebView = true;
                    getGPS();
                    String strUrl = "";
                    if (phoneModel.contains("SM-T700")) {
                        strUrl = "http://"+ServerIP+":3003/SearcMain.aspx?wa=on&BS=" + String.valueOf(BatteryLevel)
                                + "&CPU=" + String.valueOf((int) (CPU * 100)) + "&GPS=" + String.valueOf(long1) + "_" + String.valueOf(lat1);
                    }
                    else
                    {
                        strUrl = "http://"+ServerIP+":3003/SearchMain.aspx?wa=on&BS=" + String.valueOf(BatteryLevel)
                                + "&CPU=" + String.valueOf((int) (CPU * 100)) + "&GPS=" + String.valueOf(long1) + "_" + String.valueOf(lat1);
                    }
                    webview.loadUrl(strUrl);
                    Log.i("MAMmain", "BS: " + strUrl);
                }
            }
        };
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryLevelReceiver, batteryLevelFilter);
    }

    /// GPS 값 파악
    private void getGPS(){
        LocationManager mlocManager=null;
        LocationListener mlocListener;
        mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener();
        mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, mlocListener);

        if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if(MyLocationListener.latitude>0)
            {
                long1 = MyLocationListener.longitude;
                lat1 = MyLocationListener.latitude;
            }
            else
            {
                Toast.makeText(getApplicationContext(), "위치값을 수신중입니다.", Toast.LENGTH_LONG).show();
            }
        } else
        {
            Toast.makeText(getApplicationContext(), "GPS가 꺼져 있습니다.", Toast.LENGTH_LONG).show();
        }

    }

    /// Terminal Command 명령을 통해 CPU 사용량 파악
    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception e) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    private final static Object methodInvoke(Object obj, String method, Class<?>[] parameterTypes, Object[] args) {
        try {
            Method m = obj.getClass().getMethod(method, new Class[] { boolean.class });
            m.invoke(obj, args);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
     * JavaScript Interface. Web code can access methods in here
     * (as long as they have the @JavascriptInterface annotation)
     */
    public class WebViewJavaScriptInterface{

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context){
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message){
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }

        // 이미지 선택 창 처리
       @JavascriptInterface
        public void showImagePicker(){
            // Here is part of the issue, the uploadMsg is null since it is not triggered from Android
            Log.i("MAMmain", "showImagePicker()");
//            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            /// 이미지만
            i.setType("image/*");
            /// 로컬 콘텐츠만 표시
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            /// 카테고리 표시
            i.addCategory(Intent.CATEGORY_OPENABLE);
            MAMmain.this.startActivityForResult(Intent.createChooser(i, "이미지 선택"), MAMmain.FILECHOOSER_RESULTCODE);
        }

        // 비디오 선택창 처리
        @JavascriptInterface
        public void showVideoPicker(){
            // Here is part of the issue, the uploadMsg is null since it is not triggered from Android
            Log.i("MAMmain", "showVideoPicker()");
//            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            /// 비디오만
            i.setType("video/*");
            /// 로컬 콘텐츠만
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            MAMmain.this.startActivityForResult(Intent.createChooser(i, "비디오 선택"), MAMmain.FILECHOOSER_RESULTCODE);
        }

        // 비디오 재생 처리: URL을 통한 HTTP Progressive 방식
        @JavascriptInterface
        public void play1(String videoname){
            // Here is part of the issue, the uploadMsg is null since it is not triggered from Android
            Log.i("MAMmain", "play1(): " + videoname);
            Intent i = new Intent(Intent.ACTION_VIEW);
            /// 비디오 URL로 HTTP Progressive 방식으로 스트리밍 플레이함.
            i.setDataAndType(Uri.parse("http://"+ServerIP+":3000/Storage/Videos/"+videoname), "video/*");
            startActivity(Intent.createChooser(i, "Complete action using"));
        }
    }

    public class ChromeClient extends  WebChromeClient {
        //The undocumented magic method override
        //Eclipse will swear at you if you try to put @Override here
        // For Android 3.0+
       @JavascriptInterface
       public void openFileChooser(ValueCallback<Uri> uploadMsg) {

            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            MAMmain.this.startActivityForResult(Intent.createChooser(i,"File Chooser"), FILECHOOSER_RESULTCODE);
            Log.i("MAMmain", "openFileChooser(ValueCallback<Uri> uploadMsg)");
        }

       @JavascriptInterface
        // For Android 3.0+
        public void openFileChooser( ValueCallback uploadMsg, String acceptType ) {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);

        i.setType("*/*");
          MAMmain.this.startActivityForResult(
                    Intent.createChooser(i, "File Browser"),
                    FILECHOOSER_RESULTCODE);
            Log.i("MAMmain", "openFileChooser(ValueCallback uploadMsg, String acceptType)");
        }

        @JavascriptInterface
        //For Android 4.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            MAMmain.this.startActivityForResult( Intent.createChooser( i, "File Chooser" ), MAMmain.FILECHOOSER_RESULTCODE );
            Log.i("MAMmain", "openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)");
        }

        @JavascriptInterface
        // The new code
        public void showPicker( ValueCallback<Uri> uploadMsg ){
            // Here is part of the issue, the uploadMsg is null since it is not triggered from Android
            Log.i("MAMmain", "showPicker()");
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            MAMmain.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), MAMmain.FILECHOOSER_RESULTCODE);
        }
    }

    private class SendHttpRequestTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            String fileName = params[1];
            String filePath = params[2];
            String data = null;
//            Bitmap b = BitmapFactory.decodeResource(UploadActivity.this.getResources(), R.drawable.logo);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            b.compress(CompressFormat.PNG, 0, baos);

            try {
                HttpClient client = new HttpClient(url);
                client.connectForMultipart();
//				client.addFormPart("param1", param1);
//				client.addFormPart("param2", param2);
                client.addFilePart("file", fileName, filePath);
                client.finishMultipart();
                data = client.getResponse();
                Log.i("MAMmain", "Post result: " + data);
            }
            catch(Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String data) {
//            item.setActionView(null);
        }

    }


    /// 프로그래스 UI 표시
    private void showProgress(String msg)
    {
        ProgressDismissed = false;
        pbar =  ProgressDialog.show(MAMmain.this, msg, "please wait..." );
        pbar.setCancelable(true);
        new Thread(new Runnable() {
            public void run() {
                // TODO Auto-generated method stub
                int progressStatus = 0;
                /// 업로드가 완료되면 ProgressDismissed가 True로 변하여 프로그래스바 사라짐.
                while (progressStatus < 101 && !ProgressDismissed) {
                    progressStatus = (progressStatus+1) % 101;
                }
                /// 프로그래스 제거
                ProgressDismissed = false;
                pbar.dismiss();

            }
        }).start();
    }



}
