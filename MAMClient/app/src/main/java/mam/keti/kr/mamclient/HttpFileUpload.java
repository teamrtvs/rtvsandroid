package mam.keti.kr.mamclient;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import java.io.InputStream;
import java.io.OutputStream;
import android.os.StrictMode;
import android.widget.ImageView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Will on 2015-01-06.
 */
public class HttpFileUpload {



  public void HttpFileUpload(String urlString, String params, String filePath, String fileName) {

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        if(android.os.Build.VERSION.SDK_INT > 9){

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);

        }

        try {
         FileInputStream mFileInputStream = new FileInputStream(filePath);
            URL connectUrl = new URL(urlString);
           Log.d("Test", "mFileInputStream  is " + mFileInputStream);
            // open connection

            HttpURLConnection conn = (HttpURLConnection) connectUrl.openConnection();

            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setChunkedStreamingMode(102400);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

// write data
            OutputStream outputStream = conn.getOutputStream();

            DataOutputStream dos = new DataOutputStream(outputStream);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);


            int bytesAvailable = mFileInputStream.available();
            int maxBufferSize = 102400;
           //int maxBufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            Log.d("", bytesAvailable + "," +maxBufferSize);

            byte[] buffer = new byte[bufferSize];
            int bytesRead = mFileInputStream.read(buffer, 0, bufferSize);

       //     Log.d("Test", "image byte is " + bytesRead);

// read image

         while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = mFileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = mFileInputStream.read(buffer, 0, bufferSize);
            }


        dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            int totalBytes =0;
            try {
                while (bytesRead > 0) {
                    try {
                        dos.write(buffer, 0, bufferSize);
                    } catch (OutOfMemoryError e) {
                        Log.d("Test", "exception " + e.getMessage());

                    }
                    bytesAvailable =mFileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    Log.d("", bytesAvailable + "," +maxBufferSize);
                    bytesRead =mFileInputStream.read(buffer, 0, bufferSize);
                    totalBytes += bytesRead;
                }
            } catch (Exception e) {
                Log.d("Test", "exception " + e.getMessage());

            }
           dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("Server Response Message", serverResponseMessage);

            if (serverResponseCode == 200) {
                Log.d("Test", "success");
            }
// close streams

            Log.e("Test", "File is written");

            mFileInputStream.close();
            dos.flush(); // finish upload...  

// get response

            int ch;
            InputStream is = conn.getInputStream();
            StringBuffer b = new StringBuffer();

            while ((ch = is.read()) != -1) {
                b.append((char) ch);
            }
            String s = b.toString();
            Log.e("Test", "result = " + s);
            dos.close();
        } catch (Exception e) {
            Log.d("Test", "exception " + e.getMessage());
// TODO: handle exception
        }
    }
}